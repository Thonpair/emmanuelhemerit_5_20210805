/**
 *  Gestion du panier
 */

import { formatedPrice, updateBasketResume, inputValidation } from '../js/utils.js';
import Basket from "../js/basket.js";
import Order from "../js/order.js";
import { orderUrl } from '../settings.js';

const basket = new Basket();
updateBasketResume(basket);

/** Récupération de l'ensemble des produits du panier */
const products = basket.get();

/** Element sur lequel les autres éléments vont s'insérer */
const basketElement = document.getElementById("basket");

if (basket.isEmpty()) {
    document.getElementById("container").innerText = "Le panier est vide";
} else {

    /** Boucle sur la liste des produits */
    products.map(product => {
        /**Pour chaque produit, création d'une ligne qui contiendra les propriétés du produit */
        let basketProduct = document.createElement('div');
        basketProduct.className = "basket__product";
        /** Image */
        let basketProductImg = document.createElement('img');
        basketProductImg.className = "basket__product__img";
        basketProductImg.src = product.imageUrl;
        basketProductImg.alt = product.name;
        /** Nom */
        let basketProductName = document.createElement('div');
        basketProductName.className = "basket__product__name";
        basketProductName.innerText = product.name;
        /** Couleur */
        let basketProductColor = document.createElement('div');
        basketProductColor.className = "basket__product__color";
        basketProductColor.innerText = product.selectedColor;
        /** Prix, formaté en € */
        let basketProductPrice = document.createElement('div');
        basketProductPrice.className = "basket__product__price";
        basketProductPrice.innerText = formatedPrice(product.price);

        /** Ajout des propriétés à la ligne du produit */
        basketProduct.appendChild(basketProductImg);
        basketProduct.appendChild(basketProductName);
        basketProduct.appendChild(basketProductColor);
        basketProduct.appendChild(basketProductPrice);

        /** Ajout de la ligne du produit au panier */
        basketElement.appendChild(basketProduct);
    })

    /** Total du panier */
    let basketTotal = document.getElementById('basket__total');
    basketTotal.innerText += formatedPrice(basket.total());

    /** Formulaire de contact */
    let inputClass = "basket__contact__form__input"

    const basketContactFormFirstName = document.getElementById("firstname");
    basketContactFormFirstName.addEventListener('focus', () => basketContactFormFirstName.className = inputClass);

    const basketContactFormLastName = document.getElementById("lastname")
    basketContactFormLastName.addEventListener('focus', () => basketContactFormLastName.className = inputClass);

    const basketContactFormAddress = document.getElementById("address")
    basketContactFormAddress.addEventListener('focus', () => basketContactFormAddress.className = inputClass);

    const basketContactFormCity = document.getElementById("city")
    basketContactFormCity.addEventListener('focus', () => basketContactFormCity.className = inputClass);

    const basketContactFormEmail = document.getElementById("email")
    basketContactFormEmail.addEventListener('focus', () => basketContactFormEmail.className = inputClass)

    document.getElementById("reset").addEventListener('click', () => {
        basket.clear();
        updateBasketResume(basket);
    })

    document.getElementById("order").addEventListener('click', () => {
        //Variable pour les différents controles des champs inputs. Passe à false dès qu'un problème est détecté
        let control = true;
        
        // Controle des champs input
        [...document.getElementsByTagName('input')].map(input => {
            // Défini la classe par défaut à valide
            input.className = inputClass + "__valid";
            // Controle la validité du champ
            if (!inputValidation(input)){
                input.className = inputClass + "__invalid";
                // Blocage de l'envoi de la commande
                control = false;
            }
        });
        // si ok, envoi de la commande à l'api
        if (control) {
            /**Création d'un nouvel objet Order, pointant vers l'url défini et en ajoutant order */
            const order = new Order(orderUrl);
            /** Lance la commande avec le contenu des différents champs */
            order.sendOrder(
                basket.getId(),
                basketContactFormFirstName.value,
                basketContactFormLastName.value,
                basketContactFormAddress.value,
                basketContactFormCity.value,
                basketContactFormEmail.value
            ).then(data => {
                /** 
                 dès réception des données de l'api : 
                 - Nettoyage du panier
                 - Affichage de la facture
                 * */
                basket.clear();
                updateBasketResume(basket);
                document.location.href = '../facture/facture.html?id=' + data.orderId;
            })
                .catch(err => {
                    console.log(error());
                });
        }
    })
}