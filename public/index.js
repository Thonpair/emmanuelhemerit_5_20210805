/**
 * Gestion de la page d'accueil
 */

import { formatedPrice, updateBasketResume } from './js/utils.js';
import Basket from "./js/basket.js";
import { getProducts } from './js/api.js';

const basket = new Basket();
updateBasketResume(basket);

/** Element sur lequel les autres éléments vont s'insérer */
const LIST = document.getElementById('list');

/** Appel de l'api */
getProducts()
  .then(products => {
    products.forEach((product) => {
      // Création d'une div avec l'id du produit
      const card = document.createElement("a");
      card.id = product._id;
      card.href = "./produits/produits.html?id=" + product._id;
      card.className = "list__products__card";
      card.textContent = "";
      LIST.appendChild(card);

      // Création d'une div qui contiendra l'image du produit
      const card__imgDiv = document.createElement("div");
      card__imgDiv.className = "list__products__card__img";

      //Création et ajout de l'image à la div 
      const card__img = document.createElement("img");
      card__img.src = product.imageUrl;
      card__imgDiv.appendChild(card__img);

      // Création d'une div qui contiendra la description de l'article
      const descriptionProduct = document.createElement("div");
      descriptionProduct.className = "list__products__card__description";

      // Création d'une balise de titre h2 contenant le nom du produit
      const productName = document.createElement("h3");
      productName.className = "list__products__card__description__name";
      productName.textContent = product.name;

      // Création d'une balise de paragraphe contenant le prix du produit 
      const productPrice = document.createElement("p");
      productPrice.className = "list__products__card__description__price";
      productPrice.textContent = formatedPrice(product.price);

      descriptionProduct.appendChild(productName);
      descriptionProduct.appendChild(productPrice)


      // Ajout de ces balises dans la div créee en premier
      card.appendChild(card__imgDiv);
      card.appendChild(descriptionProduct);
    })
  })