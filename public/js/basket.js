export default class Basket {
    /**Gestion du panier de l'utilisateur
     * Le panier est stockée dans le localStorage du navigateur, 
     * ce qui permet de le conserver si l'utilisateur actualise ou ferme la page
     */
    constructor() {
        /**Vérification si le panier existe dans le localStorage, création si besoin */
        this.get();
    };
    /**Retourne le panier */
    get() {
        /**Vérification si le panier existe dans le localStorage, création si besoin */
        if (JSON.parse(window.localStorage.getItem('basket')) === null) {
            this.clear();
        }
        /** Retourne le contenu du panier stocké en localStorage. 
         * S'il n'existait pas, un panier vide créé précédemment sera renvoyé */
        return JSON.parse(window.localStorage.getItem('basket'));
    };

    /**Retourne les id des produits stockés dans le panier */
    getId() {
        return this.get().map(product => product.id)
    }

    /**Retourne True si le panier est vide, False si le panier contient des articles */
    isEmpty() {
        return (this.len() === 0)
    }

    /**Créé un nouveau panier vide dans le localStorage.
     * Remets à zéro un panier existant
     */
    clear() {
        window.localStorage.setItem('basket', JSON.stringify([]))
    }

    /**Ajoute un produit fourni en argument dans le localStorage du navigateur */
    add(product) {
        /** Récupération du panier actuel dans un tableau */
        const basket = this.get();
        /** Ajoute le nouveau produit à ce tableau */
        basket.push(product);
        /** Insère ce tableau modifié au panier dans le localStorage, en écrasant l'existant */
        window.localStorage.setItem('basket', JSON.stringify(basket));
    };

    /** Calcule le total des prix contenu dans le localStorage */
    total() {
        /**Initialise le résultat à 0 */
        let result = 0;
        /**Pour chaque produit, on ajoute le contenu de la variable price au résultat */
        this.get().map(item => {
            result += item.price;
        })
        /**Lorsque tous les produits ont été analysés et ajouté au résultat, 
         * on retourne ce résultat */
        return result;
    };

    /** Retourne le nombre de produit présent dans le panier */
    len() {
        return this.get().length;
    }
}