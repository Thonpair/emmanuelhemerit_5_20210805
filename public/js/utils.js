/* Formate un montant en centimes en euros, avec un séparateur virgule(,) et un symbole € */
export function formatedPrice(price) {
    return String(Number.parseFloat(price / 100).toFixed(2)).replace('.', ',') + '€';
}

/** Met à jour le résumé du panier dans l'en-tête */
export function updateBasketResume(basket) {
    /** Nombre d'éléments dans le panier */
    const basketLen = basket.len();
    /** Récupération des éléments à modifier */
    const basketQty = document.getElementById('basket-qty');
    /** Ajout de la quantité */
    basketQty.innerText = `(${basketLen})`;
}

export function inputValidation(input){
    // champs input contenant les noms / prénoms
    const inputNames = ['firstname', 'lastname'];
    //regex pour les input
    const inputLenRegex = /.{3,}/;
    //regex pour les noms / prénoms
    const inputNamesRegex = /^[a-zA-Z]{3,}$/;
    // regex pour les email
    const emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    // Controle qu'il y a du texte saisi dans l'input, en se basant sur la longueur de la valeur
    if (input.type === 'text' && !inputLenRegex.test(input.value)) {
        return false
    }
    //Controle sur les noms et prénoms
    if (inputNames.includes(input.id) && !inputNamesRegex.test(input.value)){
        return false;
    }
    // Controle sur les champs de type mail que la saisie est bien une adresse e-mail, en se basant sur une regex
    if (input.type === 'email' && !emailRegex.test(input.value)) {
        return false
    }
    return true
}