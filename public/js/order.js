export default class order {
    /**Initialise le constructeur avec l'url de l'API fourni en argument */
    constructor(url) {
        this.url = url;
    }

    /**Envoi une requête à l'API contenant la liste des id des produits,
     * avec les informations de contact.
     * Retourne une facture
     */
    sendOrder(basketId, firstName, lastName, address, city, email) {
        /**Controle que chaque argument soit correctement envoyé */
        if (!(
            /**Controle que les id soient bien un tableau non vide */
            Array.isArray(basketId) && basketId.length > 0 &&
            /** Controle que les autres champs soient du texte non vide */
            typeof (firstName) === "string" && firstName.length > 0 &&
            typeof (lastName) === "string" && lastName.length > 0 &&
            typeof (address) === "string" && address.length > 0 &&
            typeof (city) === "string" && city.length > 0 &&
            typeof (email) === "string" && email.length > 0
        )) {
            return undefined
        }
        /**Création du body qui sera transmis à la requête */
        let body = {}
        /**Ajout des id des produits */
        body.products = basketId
        /**Création d'un objet contact dans le body contenant les informations de contact */
        body.contact = {}
        /**Ajout des informations */
        body.contact.firstName = firstName
        body.contact.lastName = lastName
        body.contact.address = address
        body.contact.city = city
        body.contact.email = email
        /*Envoi de la requête */
        return new Promise((resolve, reject) => {

            /**Envoi la requête à l'adresse de l'API,
             * avec la methode POST, en indiquant qu'il s'agit de données JSON
             * et avec le contenu de l'objet body initialisé précédemment.
             * La réponse sera stockée dans la variable response
             */
            fetch(this.url, {
                method: "POST",
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(body)
            })
                /** Lorsque la réponse de l'API est arrivée, 
                 * La reponse sera retournée au format JSON
                 */
                .then(response => {
                    response.json()
                        .then(order => {
                            window.localStorage.setItem('order', JSON.stringify(order))
                            resolve(order);
                        })
                        .catch(err => reject(err))
                })
                .catch(err => reject(err))
        })
    }
    /** Récupération de la facture et de ses eléments depuis le local storage */
    getOrder(){
        return JSON.parse(window.localStorage.getItem('order'))
    }
    /** Récupération des produits de la facture */
    getOrderProducts(){
        return JSON.parse(window.localStorage.getItem('order')).products
    }
    /** Calcule le total de la facture */
    getOrderTotal(){
        let result = 0;
        JSON.parse(window.localStorage.getItem('order')).products.map(product => {
            result += product.price
        })
        return result;
    }
    /** Retourne l'id de la facture */
    getOrderId(){
        return JSON.parse(window.localStorage.getItem('order')).orderId
    }
    /** Retourne les informations du client */
    getOrderCustomer(){
        return JSON.parse(window.localStorage.getItem('order')).contact
    }
}