import { url } from '../settings.js';

/** Récupère l'ensemble des produits depuis l'api */
export function getProducts() {
    return new Promise((resolve) => {
        fetch(url)
            .then(response => response.json())
            .then(json => {
                resolve(json)
            })
    })
}

/** Récupère un produit dont l'id a été fourni en argument */
export function getProduct(productId) {
    return new Promise((resolve) => {
        fetch(url + productId)
            .then(response => response.json())
            .then(json => {
                resolve(json)
            })
    })
}