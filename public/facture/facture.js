/**
 * Gestion de la facture
 */

import Order from "../js/order.js";
import { formatedPrice } from "../js/utils.js";

/** Lecture du panier depuis LocalStorage */
const order = new Order();
const orderId = order.getOrderId();
const customer = order.getOrderCustomer();
const products = order.getOrderProducts();
const total = order.getOrderTotal();

/** Modification des valeurs de la page html */
document.getElementById('bill_id').innerText += orderId;

document.getElementById('customer_firstname').innerText = customer.firstName;
document.getElementById('customer_lastname').innerText = customer.lastName;
document.getElementById('customer_address').innerText = customer.address;
document.getElementById('customer_city').innerText = customer.city;
document.getElementById('customer_email').innerText = customer.email;

/** Insertion de chaque produit */
products.map(product => {
    const productElement = document.createElement('div');
    productElement.className = "bill__products__product";

    const productNameElement = document.createElement('div');
    productNameElement.className = "bill__products__product__name";
    productNameElement.innerText = product.name;
    productElement.appendChild(productNameElement);

    const productPriceElement = document.createElement('div');
    productPriceElement.className = "bill__products__product__price";
    productPriceElement.innerText = formatedPrice(product.price);
    productElement.appendChild(productPriceElement);
    
    document.getElementById("bill_products").appendChild(productElement);
})

/** Modification du total */
document.getElementById('bill_total').innerText += formatedPrice(total);