/** Url de l'api, peut être changé au besoin */
const API_URL = 'https://oc-p5-thonpair-production.herokuapp.com/api/teddies/';

/** url principale */
export const url = API_URL;
/** url pour la génération de facture */
export const orderUrl = API_URL + 'order';