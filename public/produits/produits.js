/**
 * Gestion de la page produit
 */

import Basket from "../js/basket.js";
import { formatedPrice, updateBasketResume } from '../js/utils.js';
import { getProduct } from '../js/api.js';

let urlId = window.location.search;
let productId = urlId.split('=')[1];

class Product {
  constructor() { }

  /** Ajoute les valeurs dans l'objet */
  setProduct(json) {
    this.id = json._id;
    this.name = json.name;
    this.price = json.price;
    this.description = json.description;
    this.imageUrl = json.imageUrl;
    this.colors = json.colors;
    this.selectedColor = json.colors[0]; //la couleur sélectionnée est la première de la liste
  };

  /**Change la couleur selectionnée */
  setSelectedColor(color) {
    /** Controle que la couleur séléectionnée soit bien dans la liste */
    if (this.colors.includes(color)) {
      /**Changement de la couleur */
      this.selectedColor = color;
    }
  };
}

/** Création des objets produit et panier */
const product = new Product();
const basket = new Basket();
updateBasketResume(basket)

/** Element sur lequel les autres éléments vont s'insérer */
let productElement = document.getElementById("product");

/** Appel de l'api */
getProduct(productId).then(jsonProduct => {
  /** Insertion des données reçues de l'API dans l'objet produit */
  product.setProduct(jsonProduct);
  /* Création d'une carte pour le produit */
  let productCard = document.createElement('div');
  productCard.className = productElement.className + "__card";
  /* Ajout de l'image à la carte */
  let img = document.createElement('img');
  img.className = productCard.className + "__img";
  img.src = product.imageUrl;
  img.alt = "photo de l'article " + product.name;
  /** Ajout de la description de l'article */
  let description = document.createElement('div');
  description.className = productCard.className + "__description";
  productCard.appendChild(description);
  /**Titre de l'article */
  let name = document.createElement('h2');
  name.className = description.className + "__name";
  name.innerText = product.name;
  /**Paragraphe contenant le prix */
  let price = document.createElement('p');
  price.className = description.className + "__price";
  /**Formatage du prix en euros */
  price.innerText = formatedPrice(product.price);
  /**Paragraphe contenant la description de l'article */
  let details = document.createElement('p');
  details.innerText = product.description;
  details.className = description.className + "__details";
  /**Ajout des boutons */
  let buttons = document.createElement('div');
  buttons.className = description.className + "__buttons";
  /**Ajout de la liste déroulante des couleurs */
  let colors = document.createElement('select');
  colors.className = buttons.className + "__color";
  colors.name = "color";
  /**Parcours de chaque couleur */
  product.colors.map(color => {
    /**Création d'une option pour le select
     * contenant une couleur
     */
    let colorOption = document.createElement('option');
    colorOption.value = color;
    colorOption.innerText = color;
    /**Ajout de la couleur au select */
    colors.appendChild(colorOption);
  })
  colors.addEventListener('change', () => product.setSelectedColor(colors.value))
  /** Création d'un bouton pour ajouter au panier */
  let buy = document.createElement('div');
  buy.className = buttons.className + "__buy";
  buy.innerText = "Ajouter au panier";
  /** Lors du clic sur ce bouton, ajout du produit au panier
   * et met à jour le résumé du panier dans l'en-tête
   */
  buy.addEventListener('click', () => {
    basket.add(product);
    updateBasketResume(basket);
  });

  /*Création de l'arborescence des éléments*/
  productCard.appendChild(img);
  description.appendChild(name);
  description.appendChild(price);
  description.appendChild(details);
  buttons.appendChild(colors);
  buttons.appendChild(buy);
  description.appendChild(buttons);
  productCard.appendChild(description);
  productElement.appendChild(productCard);
})